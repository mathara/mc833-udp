#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define IP_ADDRESS "127.0.0.1" // localhost
#define PORT	 8080
#define NET_BUF_SIZE 32

// funtion to clear buffer
void clearBuf(char* b)
{
	int i;
	for (i = 0; i < NET_BUF_SIZE; i++)
		b[i] = '\0';
}

// function to receive file
int recvFile(char* buf, int s)
{
	int i;
	char ch;
	for (i = 0; i < s; i++) {
		ch = buf[i];
		if (ch == EOF)
			return 1;
		else
			printf("%c", ch);
	}
	return 0;
}

// Driver code
int main() {
	int n, len;
	int sockfd;
	int stop;
	char sendline[NET_BUF_SIZE];
	char recvline[NET_BUF_SIZE];
	struct sockaddr_in	 servaddr;
	/*get time */
	struct timeval tv1, tv2, deltatime, deltatimeS, conndeltatime, read_timeout;
	FILE *f;

	// Creating socket file descriptor
	if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}

	//Seta um timeout
	read_timeout.tv_sec = 1;
	read_timeout.tv_usec = 0;
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);

	// zera o end do server
	memset(&servaddr, 0, sizeof(servaddr));

	// Filling server information
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORT);
	//servaddr.sin_addr.s_addr = htons(INADDR_ANY);

	//Seta o endereço IP no endserver
  // o endereço em endserver deve ser no formato interio para a função inet_pton
  inet_pton(AF_INET,IP_ADDRESS,&(servaddr.sin_addr));

	//abre o arquivo que será utilizado para armazenar os tempos de saida
	// e jogar fora resultados anteriores
	f = fopen("out_tt.txt", "w");
	while(1){
		//realiza limpeza das strings de envio e recebimento
		bzero( sendline, NET_BUF_SIZE);
		fgets(sendline,NET_BUF_SIZE,stdin); /*stdin = 0 , for standard input */

		/*time 1*/
		gettimeofday(&tv1, NULL);

		sendto(sockfd, (const char *)sendline, strlen(sendline),
			MSG_CONFIRM, (const struct sockaddr *) &servaddr,
				sizeof(servaddr));
		printf("%s\n", sendline);

		stop = 0;
		while (1) {
			stop++;
			// recebe
			clearBuf(recvline);
			n = recvfrom(sockfd, recvline, NET_BUF_SIZE,
							MSG_WAITALL, (struct sockaddr*)&servaddr,
							&len);

			if (n == -1 && stop > 9){
				printf("\n---------No Data Received---------\n");
				break;
			}
			// processo
			if (recvFile(recvline, NET_BUF_SIZE)) {
				break;
			}

		}
		/*cria arquivo da respota da consulta*/
		f = fopen("out_tt.txt", "a");

		/*time 2*/
		gettimeofday(&tv2, NULL);

		deltatime.tv_sec = tv2.tv_sec - tv1.tv_sec;
		deltatime.tv_usec = tv2.tv_usec - tv1.tv_usec;
		printf("%ld.%06ld\n", deltatime.tv_sec, deltatime.tv_usec);
		fprintf(f,"%ld.%06ld\n",deltatime.tv_sec, deltatime.tv_usec);

		/* fecha arquivo */
		fclose(f);
	}

	close(sockfd);

	return 0;
}
