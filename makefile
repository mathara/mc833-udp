CC=gcc
CFLAGS=
SQLITE=-l sqlite3

server: server.c
	$(CC) -g3 -O0 -o server server.c $(CFLAGS) $(SQLITE)

client: client.c
	$(CC) -g3 -O0 -o client client.c $(CFLAGS)
