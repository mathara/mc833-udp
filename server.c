#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <unistd.h>
#include <sqlite3.h>
#include <errno.h>

#define PORT	 8080
#define MAXLINE 1024
#define NET_BUF_SIZE 32

#define sendrecvflag 0

FILE *f;

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
	int i;

	for(i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
		fprintf(f, "%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}

	printf("\n");
	return 0;
}

// funtion to clear buffer
void clearBuf(char* b)
{
	int i;
	for (i = 0; i < NET_BUF_SIZE; i++)
		b[i] = '\0';
}

// funtion sending file
int sendFile(FILE* fp, char* buf, int s)
{
	int i;

	char ch, ch2;
	for (i = 0; i < s; i++) {
		ch = fgetc(fp);
		buf[i] = ch;
		if (ch == EOF)
			return 1;
	}
	return 0;
}

int main() {
	int len, n;
	int sockfd;
	char buffer[MAXLINE];
	char str[MAXLINE] ;
	char net_buf[NET_BUF_SIZE];
	struct sockaddr_in servaddr, cliaddr;

	// get time
	struct timeval tv1, tv2, deltatime;

	// quebra palavra
	const char s[] = ":\n ";
	char *token, *token2;

	// Teste servidor
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	char *sql;
	char qdb[150],qdb2[150];

	//Create database
	rc = sqlite3_open("test.db", &db);

	//open database
	if( rc ) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		return(0);
	} else {
		fprintf(stderr, "Opened database successfully\n");
	}

	/* Create SQL statement */
	sql = "DROP TABLE IF EXISTS Perfil;"
	"CREATE TABLE Perfil(Id INTEGER PRIMARY KEY, Email TEXT, Nome TEXT, Sobrenome TEXT, Foto TEXT, Cidade TEXT, Formacao TEXT, Habilidade TEXT, Exp TEXT);"
	"INSERT INTO Perfil VALUES (1,'maria_silva@gmail.com','Maria','Silva','C:/home/Pictures/maria.jpg','Campinas','CC','Análise de Dados','Estágio de 1 ano ');"
	"INSERT INTO Perfil VALUES (2,'msilva@gmail.com','Maria','Silva','C:/home/Pictures/maria.jpg','Campinas','CC','Análise de Dados','Estágio de 1 ano ');"
	"INSERT INTO Perfil VALUES (3,'maria_silva@gmail.com','Maria2','Silva2','C:/home/Pictures/maria.jpg','Campinas2','CC2','Análise2 de Dados','Estágio de 2 ano' );"
	"INSERT INTO Perfil VALUES (4,'jsnow@gmail.com','Jon','Snow','C:/home/Pictures/Jon.jpg','Suzano','ENF','Pensamento Lógico','(1)Estágio no HCc');"
	"INSERT INTO Perfil VALUES (5,'mtilda@gmail.com','Matilda','Bernardes','C:/home/Pictures/matilda.jpg','Americana','FEF','Liderança','(1)Jogou na LNB');"
	"INSERT INTO Perfil VALUES (6,'bsouza@gmail.com','Bianca','Souza','C:/home/Pictures/Bianca.jpg','São Paulo','MAT','Raciocínio Lógico','(1)Criação de modelos');"
	"INSERT INTO Perfil VALUES (7,'jteruel@gmail.com','Joana','Teruel','C:/home/Pictures/Joana.jpg','Três Lagoas','MAT','Gerenciamento de Equipes','(1) Scrum');"
	"INSERT INTO Perfil VALUES (8,'awei@gmail.com','Alex','Wei','C:/home/Pictures/Alex.jpg','Osasco','QUIM','Inovação Tecnológica','(1) Nanotecnologia');"
	"INSERT INTO Perfil VALUES (9,'rvero@gmail.com','Roberta','Veronez','C:/home/Pictures/Roberta.jpg','Americana','QUIM','Modelador de Soluções','(1)Oganização de Startups');"
	"INSERT INTO Perfil VALUES (10,'rcruso@gmail.com','Robson','Crusoé','C:/home/Pictures/Robson.jpg','Campinas','EQ','Inovação','(1) Nubank');"
	"INSERT INTO Perfil VALUES (11,'alesousa@gmail.com','Alessandra','Sousa','C:/home/Pictures/Alessandra.jpg','Campinas','EE','Inovação','(1) Criação de Podcast: SucessoCabeça');"
	"INSERT INTO Perfil VALUES (12,'aamazzo@gmail.com','Alexa','Amazzo','C:/home/Pictures/Alexa.jpg','Osasco','EC','Mindset criativo','(1)Coach');"
	"INSERT INTO Perfil VALUES (13,'agoncalves@gmail.com','Amanda','Gonçalves','C:/home/Pictures/Amanda.jpg','Fortaleza','EC','Scrum master','(1) Líder de projeto');"
	"INSERT INTO Perfil VALUES (14,'nloureda@gmail.com','Natália','Loureda','C:/home/Pictures/Natalia.jpg','Valinhos','AC','Cantar','(1) X Factor');"
	"INSERT INTO Perfil VALUES (15,'gmendes@gmail.com','Garbriel','Mendes','C:/home/Pictures/Gabriel.jpg','Campinas','AC','Dançar','(1) Circo de Soleil');"
	"INSERT INTO Perfil VALUES (16,'vsaldanha@gmail.com','Vitor','Saldanha','C:/home/Pictures/Vitor.jpg','Suzano','Pedago','Empatia','(1) EStágio em Escola');"
	"INSERT INTO Perfil VALUES (17,'pgono@gmail.com',' Pedro','Ono','C:/home/Pictures/Ono.jpg','Fortaleza','BIO','Metódo Cientifico','(1) Análise de Borboleta');"
	"INSERT INTO Perfil VALUES (18,'bsechin@gmail.com','Beatriz','Sechin','C:/home/Pictures/Beatriz.jpg','São Paulo','BIO','Pensamento Analítico','(1)Criação de borboletas');"
	"INSERT INTO Perfil VALUES (19,'jmoretoo@gmail.com','Julia','Moretto','C:/home/Pictures/Julia.jpg','Três Lagoas','ENF','Empatia','(1)Estágio no HC');"
	"INSERT INTO Perfil VALUES (20,'vanzazu@gmail.com','Vanessa','Zazulla','C:/home/Pictures/Vanessa.jpg','Valinhos','FEF','Cordenação de Times','(1)Atleta de alta perfromance');";
	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

	// Creating socket file descriptor
	if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}

	//preenche o valor do socket para "limpar"
	memset(&servaddr, 0, sizeof(servaddr));
	memset(&cliaddr, 1, sizeof(cliaddr));

	// Filling server information
	servaddr.sin_family = AF_INET; // IPv4
	servaddr.sin_addr.s_addr = htons(INADDR_ANY);
	servaddr.sin_port = htons(PORT);

	// Bind the socket with the server address
	if ( bind(sockfd, (const struct sockaddr *)&servaddr,
			sizeof(servaddr)) < 0 )
	{
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	/*cria arquivo da respota da consulta*/
	f = fopen("out_ts.txt", "w");
	fclose(f);

	while(1){
		//recebe mensagema
		len = sizeof(struct sockaddr);
		n = recvfrom(sockfd, (char *)buffer, MAXLINE,
					MSG_WAITALL, ( struct sockaddr *) &cliaddr,
					&len);

		buffer[n] = '\0';
		printf("Client : %s\n", buffer);

		/*tempo server 1*/
		gettimeofday(&tv1, NULL);

		//process da quebra de palavras
		token = strtok(buffer, s);
		token = strtok(NULL, s);

		//tratamento e pesquisa no banco de dados
		//lista de operacoes
		if(strcmp(buffer,"1") == 0){
			// listar todas as pessoas formadas em um determinado curso;
			sprintf(qdb,"SELECT * from Perfil where Formacao = '%s'", token);
		}
		else if(strcmp(buffer,"2") == 0){
			//listar as habilidadesdos perfis que moram em uma determinada cidade;
			sprintf(qdb,"SELECT Nome,Habilidade from Perfil where Cidade = '%s'", token);
		}
		else if(strcmp(buffer,"3") == 0){
			//acrescentaruma nova experiência em um perfil;
			token2 = strtok(NULL, s);
			sprintf(qdb,"UPDATE Perfil set Habilidade ='%s' where Email='%s' ;",token2, token);
			sprintf(qdb2,"SELECT * from Perfil where Email='%s'", token);
			strcat(qdb,qdb2);
		}
		else if(strcmp(buffer,"4") == 0){
			//dado o email do perfil,retornar sua experiência;
			strcpy(qdb,"SELECT Exp from Perfil where Email = '");
			strcat(qdb,token);
			strcat(qdb,"'");
		}
		else if(strcmp(buffer,"5") == 0){
			//listar todas as informações de todos os perfis;
			strcpy(qdb,"SELECT * from Perfil");
		}
		else if(strcmp(buffer,"6") == 0){
			//dado o email de um perfil, retornar suas informações.
			sprintf(qdb,"SELECT * from Perfil where Email = '%s'",token);
		}

		/* Execute SQL statement */
		sql = qdb;
		/*cria arquivo da respota da consulta*/
		f = fopen("file.txt", "w");
		rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

		/* fecha arquivo */
		fclose(f);
		/* erro caso a operacao tenha dado errada */
		if( rc != SQLITE_OK ) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		else {
			fprintf(stdout, "Operation done successfully\n");
		}

		/*gera arquivo com o tempo */
		f = fopen("out_ts.txt", "a");

		/*tempo server 2*/
		gettimeofday(&tv2, NULL);

		deltatime.tv_sec = tv2.tv_sec - tv1.tv_sec;
		deltatime.tv_usec = tv2.tv_usec - tv1.tv_usec;
		printf("%ld.%06ld\n", deltatime.tv_sec, deltatime.tv_usec);
		fprintf(f,"%ld.%06ld\n", deltatime.tv_sec, deltatime.tv_usec);
		fclose(f);

		f = fopen("file.txt", "r");
		while (1) {

			// processo de enviar arquivo
			if (sendFile(f, net_buf, NET_BUF_SIZE)) {
				int err = sendto(sockfd, net_buf, NET_BUF_SIZE,
					sendrecvflag,
					(struct sockaddr*)&cliaddr, len);
				if(err == -1) {
					printf("%d\n", errno);
				}
				break;
			}

			// envia
			sendto(sockfd, net_buf, NET_BUF_SIZE,
				sendrecvflag,
				(struct sockaddr*)&cliaddr, len);
			clearBuf(net_buf);
		}
		if (f != NULL)
			fclose(f);

	}
	sqlite3_close(db);

	return 0;
}
